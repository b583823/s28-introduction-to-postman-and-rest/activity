// fetch("https://jsonplaceholder.typicode.com/todos")
// .then((response) => response.json())
// .then((json) => {
// 	json.forEach(posts => console.log(posts.title));
// });
 
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(json))

// fetch("https://jsonplaceholder.typicode.com/todos")
// .then((response) => response.json())
// .then((json) => {
// 	json.forEach(todos => console.log(`The item ${todos.title} on the has a status of ${todos.completed}.`));
// });
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(`The item ${json.title} on the has a status of ${json.completed}.`));

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers:{
		"Content-Type":"application/json"
	},
	body: JSON.stringify({
		completed: "false",
		id: 201,
		userId: 1
	})

})
// response of the server base on the request
.then((response) => response.json())
.then((json) => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers:{
		"Content-Type":"application/json"
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id:1,
		title: "Updated To Do List Item",
		userId: 1
	})

})
// response of the server base on the request
.then((response) => response.json())
.then((json) => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers:{
		"Content-Type":"application/json"
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		id:1,
		status: "Complete",
		title: "delectus aut autem",
		userId: 1
	})

})
// response of the server base on the request
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
});